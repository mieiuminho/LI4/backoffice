using System;
using DelegateWise.Data;
using DelegateWise.Data.Exceptions;
using DelegateWise.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace delegatewise.Controllers
{
    public class MyController : Controller
    {
        protected readonly IDal Dal;

        public MyController()
        {
            Dal = new Dal();
        }

        public User LoggedUser()
        {
            try
            {
                return Dal.GetUser(int.Parse(HttpContext.User.Identity.Name));
            }
            catch (Exception)
            {
                throw new NotLoggedException();
            }
        }

        public async void Disconnect()
        {
            await HttpContext.SignOutAsync();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                Dal.Dispose();
            base.Dispose(disposing);
        }
    }
}
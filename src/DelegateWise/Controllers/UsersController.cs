using System;
using DelegateWise.Data.Exceptions;
using DelegateWise.Models;
using DelegateWise.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static DelegateWise.Models.SortMethod;

namespace delegatewise.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : MyController
    {
        public IActionResult Index(SortMethod? sortMethod)
        {
            return View(new ListViewModel(Dal.GetUsers(), sortMethod));
        }

        public IActionResult GenerateInvitation()
        {
            string websiteHost = Environment.GetEnvironmentVariable("DELEGATEWISE_BACKOFFICE_WEBSITE_HOST");
            ViewData["Link"] = websiteHost + "/Join/" + Dal.GenerateInvitation();
            return View("Generate");
        }

        public IActionResult Edit(int id)
        {
            try
            {
                return View(Dal.GetUser(id));
            }
            catch (UserNotFoundException)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Edit(User user)
        {
            if (!ModelState.IsValid)
                return View(user);
            try
            {
                Dal.EditUser(user);
                return RedirectToAction("Index");
            }
            catch (EmailAlreadyUsedException e)
            {
                ModelState.AddModelError("Email", e.Message);
                return View(user);
            }
        }

        public IActionResult Delete(int id)
        {
            if (LoggedUser().Id == id)
                return RedirectToAction("Index");
            try
            {
                Dal.DeleteUser(Dal.GetUser(id));
            }
            catch (UserNotFoundException)
            {
            }
            return RedirectToAction("Index");
        }
    }
}
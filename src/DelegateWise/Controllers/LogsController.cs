using System;
using System.Collections.Generic;
using DelegateWise.Data.Exceptions;
using DelegateWise.Models;
using DelegateWise.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace delegatewise.Controllers
{
    [Authorize]
    public class LogsController : MyController
    {
        public IActionResult Index()
        {
            DateTime to = DateTime.Now.AddDays(1).Date;
            DateTime from = DateTime.Now.AddDays(-6).Date;

            var logs = Dal.GetLogs(DateTime.Now.Date, to, LogType.Login);
            var dataPoints = DataPoint.FromLogs(logs, from, to);
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            return View();
        }

        [HttpPost]
        public IActionResult Index(LogsViewModel model)
        {
            DateTime from, to;
            if (model.From == null)
            {
                from = DateTime.Today.AddDays(-7);
                to = DateTime.Today;
            }
            else
            {
                from = DateTime.ParseExact(model.From, "dd/MM/yyyy", null);
                to = DateTime.ParseExact(model.To, "dd/MM/yyyy", null).AddDays(1);
            }


            var logs = Dal.GetLogs(from, to, model.LogType);
            var dataPoints = DataPoint.FromLogs(logs, from, to);
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            return View();
        }
    }
}
using DelegateWise.Data.Exceptions;
using DelegateWise.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace delegatewise.Controllers
{
    public class InscriptionController : MyController
    {
        public IActionResult Index(string hash)
        {
            try
            {
                Dal.CheckInvitation(hash);
                return View(new CreateUserViewModel(hash));
            }
            catch (ExpiredInvitationException)
            {
                return View("InvitationError");
            }
        }

        [HttpPost]
        public IActionResult Index(CreateUserViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            try
            {
                Dal.CheckInvitation(model.Invitation);
                Dal.CreateUser(model.User);
                Dal.UseInvitation(model.Invitation);
                return Redirect("/");
            }
            catch (EmailAlreadyUsedException e)
            {
                ModelState.AddModelError("User.Email", e.Message);
                return View();
            }
            catch (ExpiredInvitationException)
            {
                return View("InvitationError");
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DelegateWise.Data.Exceptions;
using DelegateWise.Models;
using DelegateWise.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace delegatewise.Controllers
{
    public class LoginController : MyController
    {
        public IActionResult Index()
        {
            try
            {
                LoggedUser();
                return View("../Logs/Index", new LogsViewModel());
            }
            catch (NotLoggedException)
            {
                return View();
            }
        }

        [HttpPost]
        public async Task<ActionResult> Index(User credentials)
        {
            try
            {
                var user = Dal.Login(credentials.Email, credentials.Password);
                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Admin ? "Admin" : "User"),
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var authProperties = new AuthenticationProperties
                {
                    AllowRefresh = true,
                    ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                    IsPersistent = true,
                    IssuedUtc = DateTimeOffset.UtcNow,
                };

                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);
                Dal.UpdateLogs();

                return RedirectToAction("Index", "Logs");
            }
            catch (WrongPasswordException)
            {
            }
            catch (UserNotFoundException)
            {
            }
            ModelState.AddModelError("User.Email", "Wrong Credentials");
            return View();
        }

        public IActionResult Logout()
        {
            Disconnect();
            return Redirect("/");
        }
    }
}
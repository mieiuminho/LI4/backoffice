using DelegateWise.Data.Exceptions;
using DelegateWise.Models;
using DelegateWise.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace delegatewise.Controllers
{
    [Authorize]
    public class AccountController : MyController
    {
        public IActionResult Index()
        {
            try
            {
                return View(new EditUserViewModel(this));
            }
            catch (NotLoggedException)
            {
                Disconnect();
                return Redirect("/");
            }
        }

        [HttpPost]
        public IActionResult Index(User user)
        {
            try
            {
                if (!LoggedUser().Equals(user))
                    return Redirect("/");
                user.Admin = LoggedUser().Admin;
                Dal.EditUser(user);
                return View(new EditUserViewModel(this) { Ok_User = true });
            }
            catch (EmailAlreadyUsedException e)
            {
                ModelState.AddModelError("User.Email", e.Message);
                return View(new EditUserViewModel(this));
            }
            catch (NotLoggedException)
            {
                Disconnect();
                return Redirect("/");
            }
        }

        [HttpPost]
        public IActionResult ChangePassword(string oldPass, string newPass)
        {
            try
            {
                var user = LoggedUser();
                if (user.Password == oldPass)
                {
                    Dal.ChangePassword(user, newPass);
                    return View("Index", new EditUserViewModel(this) { Ok_Pass = true });
                }

                ModelState.AddModelError("OldPass", "Wrong Password.");
                return View("Index", new EditUserViewModel(this));
            }
            catch (NotLoggedException)
            {
                Disconnect();
                return Redirect("/");
            }
        }
    }
}
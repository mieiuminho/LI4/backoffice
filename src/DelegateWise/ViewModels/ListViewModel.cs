using DelegateWise.Models;
using static DelegateWise.Models.SortMethod;

namespace DelegateWise.ViewModels
{
    public class ListViewModel
    {
        public Users Users { get; set; }
        public SortMethod SortMethod { get; set; }

        public ListViewModel(Users users, SortMethod? sortMethod)
        {
            Users = users;
            SortMethod = sortMethod ?? Nameasc;
            users.Sort(sortMethod);
        }

        public SortMethod SelectSort(SortMethod newSortMethod)
        {
            return newSortMethod switch
            {
                Nameasc => (SortMethod == Nameasc ? Namedsc : Nameasc),
                Emailasc => (SortMethod == Emailasc ? Emaildsc : Emailasc),
                Admin => (SortMethod == Admin ? SortMethod.Users : Admin),
                _ => Nameasc
            };
        }
    }
}
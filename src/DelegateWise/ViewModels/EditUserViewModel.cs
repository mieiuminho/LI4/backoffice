using System;
using System.ComponentModel.DataAnnotations;
using delegatewise.Controllers;
using DelegateWise.Models;

namespace DelegateWise.ViewModels
{
    public class EditUserViewModel
    {
        [Display(Name = "Old Password")] public string OldPass { get; set; }

        [Display(Name = "New Password")] public string NewPass { get; set; }

        public User User { get; set; }

        public bool Ok_User { get; set; }

        public bool Ok_Pass { get; set; }

        public EditUserViewModel(MyController controll)
        {
            User = controll.LoggedUser();
            Ok_User = false;
            Ok_Pass = false;
        }
    }
}
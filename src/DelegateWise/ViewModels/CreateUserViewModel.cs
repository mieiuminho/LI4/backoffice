using DelegateWise.Models;

namespace DelegateWise.ViewModels
{
    public class CreateUserViewModel
    {
        public string Invitation { get; set; }
        public User User { get; set; }

        // Needed
        public CreateUserViewModel()
        {
        }

        public CreateUserViewModel(string hash)
        {
            Invitation = hash;
        }
    }
}
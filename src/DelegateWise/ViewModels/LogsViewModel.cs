using System;
using delegatewise.Controllers;
using DelegateWise.Models;
using static DelegateWise.Models.LogType;

namespace DelegateWise.ViewModels
{
    public class LogsViewModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public LogType LogType { get; set; }
        public int UserId { get; set; }
    }
}
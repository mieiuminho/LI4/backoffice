using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DelegateWise.Models
{
    [DataContract]
    public class DataPoint
    {
        public DataPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        [DataMember(Name = "x")]
        public Nullable<double> X = null;
        [DataMember(Name = "y")]
        public Nullable<double> Y = null;

        public static List<DataPoint> FromLogs(List<Log> l, DateTime min, DateTime max)
        {
            int days = (max - min).Days;

            List<int> toInts = new List<int>(new int[days]);
            foreach (Log log in l)
                toInts[(log.DateTime.Date - min).Days]++;

            List<DataPoint> points = new List<DataPoint>();
            for (int i = 0; i < days; i++)
                points.Add(new DataPoint(new DateTimeOffset(min.AddDays(i)).ToUnixTimeMilliseconds(), toInts[i]));
            return points;
        }
    }
}
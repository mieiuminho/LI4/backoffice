using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DelegateWise.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The user needs a full name.")]
        [Display(Name = "Full Name")]
        [StringLength(50, MinimumLength = 3)]
        public string FullName { get; set; }

        [Required(ErrorMessage = "The user needs a valid e-mail address.")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "The user needs a password.")]
        [PasswordPropertyText]
        public string Password { get; set; }

        public bool Admin { get; set; }

        protected bool Equals(User other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((User)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, FullName, Email, Password, Admin);
        }
    }
}
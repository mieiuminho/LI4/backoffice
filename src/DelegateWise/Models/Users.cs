using System;
using System.Collections.Generic;
using System.Linq;
using static DelegateWise.Models.SortMethod;

namespace DelegateWise.Models
{
    public enum SortMethod
    {
        Nameasc,
        Namedsc,
        Admin,
        Users,
        Emailasc,
        Emaildsc
    }

    public class Users
    {
        public List<User> UsersList { get; set; }

        public Users(IQueryable<User> users)
        {
            if (users == null) throw new ArgumentNullException(nameof(users));
            UsersList = users.ToList();
        }

        public void Sort(SortMethod? sortMethodv)
        {
            var sortMethod = sortMethodv ?? Nameasc;

            switch (sortMethod)
            {
                case Nameasc:
                    UsersList = UsersList.OrderBy(u => u.FullName).ToList();
                    break;
                case Namedsc:
                    UsersList = UsersList.OrderByDescending(u => u.FullName).ToList();
                    break;
                case Emailasc:
                    UsersList = UsersList.OrderBy(u => u.Email).ToList();
                    break;
                case Emaildsc:
                    UsersList = UsersList.OrderByDescending(u => u.Email).ToList();
                    break;
                case Admin:
                    UsersList = UsersList.OrderBy(u => u.Admin).ToList();
                    break;
                case SortMethod.Users:
                    UsersList = UsersList.OrderByDescending(u => u.Admin).ToList();
                    break;
            }
        }

        public int Count()
        {
            return UsersList.Count;
        }
    }
}
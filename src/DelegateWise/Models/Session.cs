using System;

namespace DelegateWise.Models
{
    public class Session
    {
        public int Id { get; set; }
        public int UserAccountId { get; set; }
        public DateTime LoginDateTime { get; set; }
        public DateTime LogoutDateTime { get; set; }
    }
}
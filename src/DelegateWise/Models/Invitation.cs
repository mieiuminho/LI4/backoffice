using System;

namespace DelegateWise.Models
{
    public class Invitation
    {
        private const int ValidHoursInvitation = 3;

        public int Id { get; set; }
        public string Hash { get; set; }
        private DateTime LimitDateTime { get; set; }

        public Invitation(string hash)
        {
            Hash = hash;
            LimitDateTime = DateTime.Now.AddHours(ValidHoursInvitation);
        }

        public bool IsValid()
        {
            return DateTime.Compare(DateTime.Now, LimitDateTime) <= 0;
        }
    }
}
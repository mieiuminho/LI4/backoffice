using System;

namespace DelegateWise.Models
{
    public enum LogType
    {
        Login,
        RegisterAccount,
        DeleteAccount,
        CreateWorkspace,
        DeleteWorkspace,
        TaskDone,
        UserReported
    }


    public class Log
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public LogType Type { get; set; }
        public DateTime DateTime { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using DelegateWise.Data.Exceptions;
using DelegateWise.Models;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;

namespace DelegateWise.Data
{
    public class Dal : IDal
    {
        protected DelegateWiseContext Db = new DelegateWiseContext();

        public void Dispose()
        {
            Db.Dispose();
        }

        public User Login(string email, string password)
        {
            var user = Db.Users.FirstOrDefault(u =>
                u.Email.Equals(email));
            if (user is null)
                throw new UserNotFoundException(NotFoundBy.Email);
            if (!user.Password.Equals(password))
                throw new WrongPasswordException();
            return user;
        }

        public void AvailableEmail(string email)
        {
            var user = Db.Users.FirstOrDefault(u =>
                u.Email.Equals(email));
            if (user != null)
                throw new EmailAlreadyUsedException();
        }

        public void EditUser(User user)
        {
            var userDb = GetUser(user.Id);
            if (userDb.Email != user.Email)
                AvailableEmail(user.Email);
            user.Password = userDb.Password;
            Db.Entry(userDb).CurrentValues.SetValues(user);
            Db.SaveChanges();
        }

        public void ChangePassword(User u, string newPass)
        {
            var userDb = GetUser(u.Id);
            userDb.Password = newPass;
            Db.Entry(userDb).CurrentValues.SetValues(userDb);
            Db.SaveChanges();
        }

        public User GetUser(int id)
        {
            var userDb = Db.Users.FirstOrDefault(u =>
                u.Id.Equals(id));
            if (userDb is null)
                throw new UserNotFoundException(NotFoundBy.Id);
            return userDb;
        }

        public Users GetUsers()
        {
            return new Users(Db.Users);
        }

        public Users GetUsers(bool admin)
        {
            var users = from u in Db.Users
                        where u.Admin == admin
                        select u;
            return new Users(users);
        }

        public Users GetUsers(string search)
        {
            var users = from u in Db.Users
                        where u.FullName.ToLower().Contains(search.ToLower())
                        select u;
            return new Users(users);
        }

        public Users GetUsers(string search, bool admin)
        {
            var users = from u in Db.Users
                        where u.Admin == admin
                        where u.FullName.ToLower().Contains(search.ToLower())
                        select u;
            return new Users(users);
        }

        public void CreateUser(User user)
        {
            AvailableEmail(user.Email);
            user.Admin = false;
            Db.Users.Add(user);
            Db.SaveChanges();
        }

        public void DeleteUser(User user)
        {
            Db.Users.Remove(user);
            Db.SaveChanges();
        }

        public void AddLog(string userId, LogType type, DateTime dateTime)
        {
            Db.Logs.Add(new Log { Type = type, DateTime = dateTime });
            Db.SaveChanges();
        }

        public void AddLog(string message, DateTime dateTime)
        {
            var type = LogType.Login;
            var userId = "";
            if (message.Contains("Login with"))
            {
                var split = message.Split(" ");
                userId = split[^1];
                type = LogType.Login;
            }
            else if (message.Contains("Task completed"))
            {
                type = LogType.TaskDone;
            }
            else if (message.Contains("New workspace"))
            {
                type = LogType.CreateWorkspace;
            }
            else if (message.Contains("Registered account"))
            {
                type = LogType.RegisterAccount;
            }
            else if (message.Contains("TODO"))
            {
                type = LogType.DeleteAccount;
            }
            else if (message.Contains("TODO"))
            {
                type = LogType.DeleteWorkspace;
            }
            else
            {
                type = LogType.UserReported;
            }

            Db.Logs.Add(new Log { UserId = userId, Type = type, DateTime = dateTime });
        }

        public void UpdateLogs()
        {
            var conn = new MySqlConnection(ConnectionStringBuilder.Build());
            conn.Open();
            var cmd = new MySqlCommand("SELECT * FROM logsbackend WHERE LEVEL = 'STATS'", conn);
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                    AddLog(reader["MESSAGE"].ToString(), DateTime.Parse(reader["DATE"].ToString() ?? string.Empty));
            Db.SaveChanges();
            cmd = new MySqlCommand("DELETE FROM logsbackend where LEVEL = 'STATS'", conn);
            cmd.ExecuteReader();
            conn.Close();
        }

        public List<Log> GetLogs(LogType type)
        {
            var logs = from l in Db.Logs
                       where l.Type == type
                       select l;
            return logs.ToList();
        }

        public List<Log> GetLogs(DateTime thisDateTime, DateTime andThisDateTime, LogType type)
        {
            var logs = from l in Db.Logs
                       where type == l.Type &&
                             DateTime.Compare(l.DateTime, thisDateTime) >= 0 &&
                             DateTime.Compare(l.DateTime, andThisDateTime) <= 0
                       select l;
            return logs.ToList();
        }

        public string GenerateInvitation()
        {
            var hash = Guid.NewGuid().ToString().Substring(0, 8);
            Db.Invitations.Add(new Invitation(hash));
            Db.SaveChanges();
            return hash;
        }

        public void CheckInvitation(string hash)
        {
            foreach (var i in Db.Invitations)
                if (!i.IsValid())
                    Db.Invitations.Remove(i);
            Db.SaveChanges();

            var inv = Db.Invitations.FirstOrDefault(i => i.Hash == hash);
            if (inv is null) throw new ExpiredInvitationException();
        }

        public void UseInvitation(string hash)
        {
            var inv = Db.Invitations.FirstOrDefault(i => i.Hash == hash);
            Db.Invitations.Remove(inv ?? throw new ExpiredInvitationException());
            Db.SaveChanges();
        }

        public void DropData()
        {
            DropUsers();
            DropLogs();
            DropInvitations();
        }

        public virtual void DropInvitations()
        {
            Db.Database.ExecuteSqlRaw("TRUNCATE TABLE Invitations");
        }

        public virtual void DropUsers()
        {
            Db.Database.ExecuteSqlRaw("TRUNCATE TABLE Users");
        }

        public virtual void DropLogs()
        {
            Db.Database.ExecuteSqlRaw("TRUNCATE TABLE Logs");
        }


    }
}
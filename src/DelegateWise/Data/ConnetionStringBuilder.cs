using System;

namespace DelegateWise.Data
{
    public static class ConnectionStringBuilder
    {
        private static readonly string DB_PORT = Environment.GetEnvironmentVariable("DELEGATEWISE_BACKOFFICE_DB_PORT");
        private static readonly string DB_HOST = Environment.GetEnvironmentVariable("DELEGATEWISE_BACKOFFICE_DB_HOST");
        private static readonly string DB_USER = Environment.GetEnvironmentVariable("DELEGATEWISE_BACKOFFICE_DB_USER");
        private static readonly string DB_NAME = Environment.GetEnvironmentVariable("DELEGATEWISE_BACKOFFICE_DB_NAME");
        private static readonly string DB_PASS = Environment.GetEnvironmentVariable("DELEGATEWISE_BACKOFFICE_DB_PASS");

        public static string Build()
        {
            return $"server={DB_HOST};port={DB_PORT};database={DB_NAME};uid={DB_USER};pwd={DB_PASS}";
        }
    }
}
using System;
using DelegateWise.Models;
using Microsoft.EntityFrameworkCore;

namespace DelegateWise.Data
{
    public class DalMemory : Dal
    {

        public DalMemory()
        {
            var options = new DbContextOptionsBuilder<DelegateWiseContext>()
                .UseInMemoryDatabase(databaseName: "DB_DELEGATEWISE_TEST")
                .Options;
            Db = new DelegateWiseContext(options);
        }

        public static void InitData()
        {
            using IDal dal = new DalMemory();
            var user = new User() { FullName = "Celso Rodrigues", Email = "celso@gmail.com", Password = "123" };
            dal.CreateUser(user);
            user.Admin = true;
            dal.EditUser(user);
            dal.CreateUser(new User() { FullName = "Hugo Rodrigues", Email = "hugo@gmail.com", Password = "123" });
            dal.CreateUser(new User() { FullName = "Pedro Rodrigues", Email = "pedro@gmail.com", Password = "123" });
            dal.CreateUser(new User() { FullName = "Joao Rodrigues", Email = "joao@gmail.com", Password = "123" });
        }

        public override void DropInvitations()
        {
            foreach (var u in Db.Invitations)
                Db.Invitations.Remove(u);
            Db.SaveChanges();
        }

        public override void DropUsers()
        {
            foreach (var user in GetUsers().UsersList)
                Db.Users.Remove(user);
            Db.SaveChanges();
        }

        public override void DropLogs()
        {
            foreach (var log in Db.Logs)
                Db.Logs.Remove(log);
            Db.SaveChanges();
        }
    }
}
using System;
using System.Collections.Generic;
using DelegateWise.Models;

namespace DelegateWise.Data
{
    public interface IDal : IDisposable
    {
        // Methods to manipulate users of the webapp
        User Login(string email, string password);
        void AvailableEmail(string email);
        void CreateUser(User user);
        void EditUser(User user);
        void DeleteUser(User user);
        void ChangePassword(User u, string newPass);
        User GetUser(int id);
        Users GetUsers();
        Users GetUsers(string search);
        Users GetUsers(bool admin);
        Users GetUsers(string search, bool admin);

        // Methods to manipulate logs
        void AddLog(string userId, LogType type, DateTime dateTime);
        void AddLog(string message, DateTime dateTime);
        List<Log> GetLogs(LogType type);
        List<Log> GetLogs(DateTime thisDateTime, DateTime andThisDateTime, LogType type);

        // Methods to manage invitations
        string GenerateInvitation();
        void CheckInvitation(string hash);
        void UseInvitation(string hash);

        void UpdateLogs();

        // Methods to manage database 
        void DropData();
        void DropInvitations();
        void DropUsers();
        void DropLogs();
    }
}
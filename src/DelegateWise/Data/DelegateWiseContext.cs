﻿using DelegateWise.Models;
using Microsoft.EntityFrameworkCore;

namespace DelegateWise.Data
{
    public class DelegateWiseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Invitation> Invitations { get; set; }

        public DelegateWiseContext(DbContextOptions<DelegateWiseContext> options) : base(options)
        {
        }

        public DelegateWiseContext() : base()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(ConnectionStringBuilder.Build());
            }
        }
    }
}
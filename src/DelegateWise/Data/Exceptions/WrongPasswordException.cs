using System;

namespace DelegateWise.Data.Exceptions
{
    public class WrongPasswordException : Exception
    {
        public WrongPasswordException() : base("Wrong password.")
        {
        }
    }
}
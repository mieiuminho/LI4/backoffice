using System;

namespace DelegateWise.Data.Exceptions
{
    public enum NotFoundBy
    {
        Email,
        Id
    }

    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(NotFoundBy a)
            : base("User with this " + (a == NotFoundBy.Email ? "e-mail" : "id") + " could not be found.")
        {
        }
    }
}
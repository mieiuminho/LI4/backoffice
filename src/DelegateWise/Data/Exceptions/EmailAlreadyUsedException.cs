using System;

namespace DelegateWise.Data.Exceptions
{
    public class EmailAlreadyUsedException : Exception
    {
        public EmailAlreadyUsedException() : base("User with this Email already exists.")
        {
        }
    }
}
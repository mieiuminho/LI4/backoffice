[hugo]: https://github.com/HugoCarvalho99
[hugo-pic]: https://github.com/HugoCarvalho99.png?size=120
[nelson]: https://github.com/nelsonmestevao
[nelson-pic]: https://github.com/nelsonmestevao.png?size=120
[pedro]: https://github.com/pedroribeiro22
[pedro-pic]: https://github.com/pedroribeiro22.png?size=120
[celso]: https://github.com/Basto97
[celso-pic]: https://github.com/Basto97.png?size=120
[ricardo]: https://github.com/ricardoslv
[ricardo-pic]: https://github.com/ricardoslv.png?size=120

<div align="center">
    <img src="src/DelegateWise/wwwroot/img/logo.png" alt="delegatewise" width="350px">
</div>

## :rocket: Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes.

### :inbox_tray: Prerequisites

The following software is required to be installed on your system:

- [.NET Core SDK](https://dotnet.microsoft.com/download/dotnet-core/3.1)
- [MySQL](https://dev.mysql.com/downloads/)

After pulling the project use theses 2 command lines to install all .NET
dependencies and tools used.

```bash
dotnet restore
dotnet tool restore
```

This project uses a MySQL database for data persistence. You should have MySQL
up and running. And a new database must be created.

For setting up a successful connection, the environment variables must be set.

```bash
cp .env.sample .env
```

### :hammer: Development

Build the application.

```bash
dotnet build
```

Starting the development server and then navigate to <https://localhost:5001>.

```bash
dotnet run --project src/DelegateWise
```

Run the tests.

```bash
dotnet test
```

Format the code.

```bash
dotnet format
```

Clean the directory.

```bash
dotnet clean
```

### :package: Deployment

Bundling the app for production.

```bash
dotnet publish
```

### :hammer_and_wrench: Tools

The recommended Integrated Development Environment (IDE) is
[Rider](https://www.jetbrains.com/rider/).

To manage the database use the [Entity
Framework](https://docs.microsoft.com/en-us/ef/core/).

## :busts_in_silhouette: Team

| [![Hugo][hugo-pic]][hugo] | [![Nelson][nelson-pic]][nelson] | [![Pedro][pedro-pic]][pedro] | [![Celso][celso-pic]][celso] | [![Ricardo][ricardo-pic]][ricardo] |
| :-----------------------: | :-----------------------------: | :--------------------------: | :--------------------------: | :--------------------------------: |
|   [Hugo Carvalho][hugo]   |    [Nelson Estevão][nelson]     |    [Pedro Ribeiro][pedro]    |   [Celso Rodrigues][celso]   |      [Ricardo Silva][ricardo]      |
